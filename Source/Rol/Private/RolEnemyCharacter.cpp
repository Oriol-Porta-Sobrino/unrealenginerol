// Fill out your copyright notice in the Description page of Project Settings.

#include "RolEnemyCharacter.h"
#include "Components/BoxComponent.h"

ARolEnemyCharacter::ARolEnemyCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	mpcollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	mpcollisionBox->SetupAttachment(RootComponent);
}

void ARolEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ARolEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARolEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}