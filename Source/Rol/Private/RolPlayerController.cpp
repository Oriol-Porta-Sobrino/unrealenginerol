// Copyright Epic Games, Inc. All Rights Reserved.

#include "Rol/Public/RolPlayerController.h"
#include "Rol/Public/RolCharacter.h"

#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraFunctionLibrary.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Rol/Public/InputConfigData.h"
#include "Rol/Public/Utils.h"

ARolPlayerController::ARolPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	FollowTime = 0.f;
}

void ARolPlayerController::BeginPlay()
{
	mpRolCharacter = Cast<ARolCharacter>(GetCharacter());

	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}

	GetClassData();

	// Call the base class  
    Super::BeginPlay();
}

void ARolPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent))
	{
		// Setup mouse input events
		EnhancedInputComponent->BindAction(inputConfigData->SetDestinationClickAction, ETriggerEvent::Started, this, &ARolPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(inputConfigData->SetDestinationClickAction, ETriggerEvent::Triggered, this, &ARolPlayerController::OnSetDestinationTriggered);
		EnhancedInputComponent->BindAction(inputConfigData->SetDestinationClickAction, ETriggerEvent::Completed, this, &ARolPlayerController::OnSetDestinationReleased);
		EnhancedInputComponent->BindAction(inputConfigData->SetDestinationClickAction, ETriggerEvent::Canceled, this, &ARolPlayerController::OnSetDestinationReleased);
		EnhancedInputComponent->BindAction(inputConfigData->mpInputAction, ETriggerEvent::Completed, this, &ARolPlayerController::OnActionPressed);

		// Setup touch input events
		/*EnhancedInputComponent->BindAction(inputConfigData->SetDestinationTouchAction, ETriggerEvent::Started, this, &ARolPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(inputConfigData->SetDestinationTouchAction, ETriggerEvent::Triggered, this, &ARolPlayerController::OnTouchTriggered);
		EnhancedInputComponent->BindAction(inputConfigData->SetDestinationTouchAction, ETriggerEvent::Completed, this, &ARolPlayerController::OnTouchReleased);
		EnhancedInputComponent->BindAction(inputConfigData->SetDestinationTouchAction, ETriggerEvent::Canceled, this, &ARolPlayerController::OnTouchReleased);*/

		for (int ButtonIndex {0}; ButtonIndex < inputConfigData->mInputSkills.Num(); ++ButtonIndex)
		{
			EnhancedInputComponent->BindAction(inputConfigData->mInputSkills[ButtonIndex], ETriggerEvent::Completed, this, &ARolPlayerController::OnSkillPressed, ButtonIndex);
		}
	}
}

void ARolPlayerController::GetClassData()
{
	if (mClassDB)
	{
		//Esto es para pillar de la base de datos
		// Transformar el enum en un String para buscar en la base de datos (es case sensitive)
		FName ClassString {UEnum::GetDisplayValueAsText(mpRolCharacter->mCharacterClass).ToString()};
		// En caso de error, si bWarnIfRowMissing is true se notificara en los output log
		static const FString FindContext {FString("Searching for").Append(ClassString.ToString())};
		// Busqueda en base de datos que castea al tipo que yo le diga, en este caso FSkillDataRow
		FClassDataRow* ClassFound = mClassDB->FindRow<FClassDataRow>(ClassString, FindContext, true);

		if(ClassFound)
		{
			mpRolCharacter->GetCharacterMovement()->MaxWalkSpeed = ClassFound->Speed;
			mpRolCharacter->mHealth = ClassFound->Health;
		}
	}
}

void ARolPlayerController::OnInputStarted()
{
	StopMovement();
}

// Triggered every frame when the input is held down
void ARolPlayerController::OnSetDestinationTriggered()
{
	// We flag that the input is being pressed
	FollowTime += GetWorld()->GetDeltaSeconds();
	
	// We look for the location in the world where the player has pressed the input
	FHitResult Hit;
	bool bHitSuccessful = false;
	if (bIsTouch)
	{
		bHitSuccessful = GetHitResultUnderFinger(ETouchIndex::Touch1, ECollisionChannel::ECC_Visibility, true, Hit);
	}
	else
	{
		bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);
	}

	// If we hit a surface, cache the location
	if (bHitSuccessful)
	{
		CachedDestination = Hit.Location;
	}
	
	// Move towards mouse pointer or touch
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn != nullptr)
	{
		FVector WorldDirection = (CachedDestination - ControlledPawn->GetActorLocation()).GetSafeNormal();
		ControlledPawn->AddMovementInput(WorldDirection, 1.0, false);
	}
}

void ARolPlayerController::OnSetDestinationReleased()
{
	// If it was a short press
	if (FollowTime <= ShortPressThreshold)
	{
		GetHitResultUnderCursor(ECC_Visibility, true, mHitResult);
		
		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}

	FollowTime = 0.f;
}

// Triggered every frame when the input is held down
void ARolPlayerController::OnTouchTriggered()
{
	bIsTouch = true;
	OnSetDestinationTriggered();
}

void ARolPlayerController::OnTouchReleased()
{
	bIsTouch = false;
	OnSetDestinationReleased();
}

void ARolPlayerController::OnSkillPressed(int aButtonPressed)
{
	if (auto SkillPressed {mSkills[aButtonPressed]}; SkillPressed != ESkill::NONE)
	{
		ScreenD(Format1("Pressed: %d", aButtonPressed));
		if (auto* Skill {GetSkill(SkillPressed)}; Skill != nullptr)
		{
			ScreenD(Format1("Skill: %s", *Skill->Description));
			mSkillSelected = *Skill;
		}
	}
}

void ARolPlayerController::OnActionPressed(const FInputActionValue& aValue)
{
	if (mSkillSelected.Name == ESkill::NONE)
		return;
	FVector HitLocation {FVector::ZeroVector};
	GetHitResultUnderCursorByChannel(TraceTypeQuery1, true, mHitResult);
	//Si quiero poder apuntar a actores debo comprobar si el actor al que he pulsado es hitable
	evOnLocationClick.Broadcast(mHitResult.Location, mSkillSelected);
}

FSkillDataRow* ARolPlayerController::GetSkill(ESkill aSkill)
{
	FSkillDataRow* SkillFound {};
	if (mSkillDB)
	{
		//Esto es para pillar de la base de datos
		// Transformar el enum en un String para buscar en la base de datos (es case sensitive)
		FName SkillString {UEnum::GetDisplayValueAsText(aSkill).ToString()};
		// En caso de error, si bWarnIfRowMissing is true se notificara en los output log
		static const FString FindContext {FString("Searching for").Append(SkillString.ToString())};
		// Busqueda en base de datos que castea al tipo que yo le diga, en este caso FSkillDataRow
		SkillFound = mSkillDB->FindRow<FSkillDataRow>(SkillString, FindContext, true);
		
	}
	return SkillFound;
}