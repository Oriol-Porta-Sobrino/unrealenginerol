// Copyright Epic Games, Inc. All Rights Reserved.

#include "Rol.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Rol, "Rol" );

DEFINE_LOG_CATEGORY(LogRol)
 