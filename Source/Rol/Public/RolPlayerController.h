// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "RolCharacter.h"
#include "SkillDataRow.h"
#include "ClassDataRow.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerController.h"
#include "RolPlayerController.generated.h"

/** Forward declaration to improve compiling times */
class UNiagaraSystem;
struct FInputActionValue;
class UInputConfigData;
class UInputMappingContext;


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLocationClick, FVector, aClickLocation, const FSkillDataRow&, aSkillData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDirectionSkillCast, FRotator, aRotation, FSkillDataRow, aSkillData);

UCLASS()
class ARolPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ARolPlayerController();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		ARolCharacter* mpRolCharacter;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
		UInputMappingContext* DefaultMappingContext;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
		UInputConfigData* inputConfigData;

#pragma region SKILLS
	FSkillDataRow* GetSkill(ESkill skill);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=mSKILLS)
		TArray<TEnumAsByte<ESkill>> mSkills;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=mCLASS)
		UDataTable* mClassDB;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=mSKILLS)
		UDataTable* mSkillDB;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=mSKILLS)
		FSkillDataRow mSkillSelected;
#pragma endregion

#pragma region EVENTS
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnLocationClick evOnLocationClick;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnDirectionSkillCast evOnDirectionSkillCast;
#pragma endregion
	
	/** Input Action for SetDestination action */	
protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	virtual void SetupInputComponent() override;

	void GetClassData();
	// To add mapping context
	virtual void BeginPlay();

	/** Input handlers for SetDestination action. */
	void OnInputStarted();
	void OnSetDestinationTriggered();
	void OnSetDestinationReleased();
	void OnTouchTriggered();
	void OnTouchReleased();

	void OnSkillPressed(int aButtonPressed);
	void OnActionPressed(const FInputActionValue& aValue);

private:
	FVector CachedDestination;
	FHitResult mHitResult;
	bool bIsTouch; // Is it a touch device
	float FollowTime; // For how long it has been pressed
};


