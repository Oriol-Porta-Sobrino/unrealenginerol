// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RolGameMode.generated.h"

UCLASS(minimalapi)
class ARolGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARolGameMode();
};



