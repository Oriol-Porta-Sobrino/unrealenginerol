// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RolEnemyCharacter.generated.h"

UCLASS()
class ROL_API ARolEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ARolEnemyCharacter();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UPROPERTY(EditDefaultsOnly)
	class UBoxComponent* mpcollisionBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float mHealth = 100.0f;

protected:
	virtual void BeginPlay() override;

};
