// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SkillBase.generated.h"

UCLASS()
class ROL_API ASkillBase : public AActor
{
	GENERATED_BODY()
	
public:
	virtual void Tick(float DeltaTime) override;

	ASkillBase();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(ExposeOnSpawn=true))
		AActor* mpTarget;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(ExposeOnSpawn=true))
		float aDamage;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(ExposeOnSpawn=true))
		float mDuration;
protected:
	virtual void BeginPlay() override;
	
};
