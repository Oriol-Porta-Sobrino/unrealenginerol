// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InputConfigData.generated.h"

class UInputAction;
/**
 * 
 */
UCLASS()
class ROL_API UInputConfigData : public UDataAsset
{
	GENERATED_BODY()

public:
	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
		UInputAction* SetDestinationClickAction;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
		UInputAction* SetDestinationTouchAction;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<UInputAction*> mInputSkills;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UInputAction* mpInputAction;
};
