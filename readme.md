Risketos Bàsics								(60%)

- [X] Un sistema preparat per tenir diverses classes, només se'n requereix un però ha d'estar preparat per ampliar, per exemple mag. En aquest ha de ser l'estructura de dades per a les estadístiques del jugador,
- [X] vida, manà, energia, dany físic, dany magic, etc. Això és al gust de cada joc.
- [X] Un sistema de combat que et permeti atacar, llançar habilitats i rebre mal.
- [X] Clarament un enemic que també ho pugui fer.
- [X] Un sistema d'habilitats que permeti al jugador canviar quina habilitat fer servir, ha de tenir com a mínim 2.

Risketos Opcionals							(30%)

- [] Sistema d’inventari amb objectes que deixen els enemics.
- [] Relacionat amb l’anterior un sistema d’equipació que modifiqui les teves estadístiques.
- [] Mapa o minimapa que mostres a on estàs.
- [] Sistema d’animacions avançat amb BlendTrees (recomano utilitzar mixamo)
- [] Sistema de nivellat amb estadístiques.
